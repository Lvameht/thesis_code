adb -d root
adb -d shell pm uninstall --user 0 com.mediatek.ims
adb -d shell pm uninstall --user 0 com.google.android.youtube
adb -d shell pm uninstall --user 0 com.google.android.googlequicksearchbox
adb -d shell pm uninstall --user 0 com.google.android.apps.messaging
adb -d shell pm uninstall --user 0 com.android.vending
adb -d shell pm uninstall --user 0 com.google.android.apps.work.oobconfig
adb -d shell pm uninstall --user 0 com.google.android.gm
adb -d shell pm uninstall --user 0 com.google.android.apps.tachyon
adb -d shell pm uninstall --user 0 com.google.android.music
adb -d shell pm uninstall --user 0 com.google.android.apps.nbu.files
adb -d shell pm uninstall --user 0 com.google.android.apps.docs
adb -d shell pm uninstall --user 0 com.google.android.apps.maps
adb -d shell pm disable --user 0 com.google.android.gms
adb -d shell pm disable com.google.android.gms
adb -d shell pm uninstall --user 0 com.google.android.gms
adb -d shell pm uninstall --user 0 com.google.android.ims
adb -d shell pm uninstall --user 0 com.google.android.tts
adb -d shell pm uninstall --user 0 com.google.android.videos
adb -d shell pm uninstall --user 0 com.google.android.apps.photos
adb -d shell pm uninstall --user 0 com.google.android.inputmethod.latin
adb -d shell settings put global window_animation_scale 0
adb -d shell settings put global transition_animation_scale 0
adb -d shell settings put global animator_duration_scale 0
adb -d shell settings put system screen_off_timeout 3600000
adb -d shell settings put global airplane_mode_on 1
adb -d push ./apks /data
adb -d shell pm install /data/apks/keyboard.apk
adb -d shell reboot