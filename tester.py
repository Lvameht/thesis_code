import csv
import time
import os
import subprocess
from subprocess import Popen, PIPE
import json
from enum import Enum
import random
import string
import requests
import sys
from pathlib import Path


def s_to_mmss(sec):
    if sec == -1:
        return "inf"
    return "{}m {}s".format(sec // 60, sec % 60)


notify_url = "https://notify.run/EhzUlu9Zp2eIgoXj"


def notify(message):
    try:
        response = requests.post(notify_url, data=message)
        if response.status_code == 200:
            print("Message sent!")
        else:
            print("Message not sent! Code {}".format(response.status_code))
    except Exception as e:
        print("Error while sending message!", e)


class ADBHandler:
    ADB_PATH = "D:\\Software\\adb\\platform-tools\\adb.exe"

    sensors = {
        "current": '/sys/class/power_supply/battery/current_now',
        "voltage": '/sys/class/power_supply/battery/voltage_now',
        "batt_lvl": '/sys/class/power_supply/battery/capacity',
        "cpu_t": '/sys/class/thermal/thermal_zone1/temp',
        "batt_t": '/sys/class/thermal/thermal_zone13/temp',
        "charger_state": '/sys/class/power_supply/charger/online'
    }

    _loggerPID = None
    _loggerFileName = ""

    def __init__(self, dev_addr, dev_tag):
        self.device_address = dev_addr
        self.device_tag = dev_tag
        os.environ["ANDROID_SERIAL"] = device_addr
        pass

    def adb_cmd_sync(self, command, retry=True):
        ret = os.system("{} {}".format(self.ADB_PATH, command))
        if ret:
            if retry:
                times_ret = 0
                while ret:
                    time.sleep(2)
                    ret = os.system("{} {}".format(self.ADB_PATH, command))
                    times_ret += 1
                    if times_ret >= 5:
                        notify("ADB error on " + self.device_tag)
                        raise (RuntimeError("ADB error"))
            else:
                raise (RuntimeError("ADB error"))

    def uninstall_apk(self, package_name: str):
        try:
            self.adb_cmd_sync("shell pm clear " + package_name, False)
            self.adb_cmd_sync("uninstall " + package_name, False)
            print("Package {} uninstalled!".format(package_name))
        except RuntimeError:
            print("Package {} is not installed!".format(package_name))

    def exec_actions(self, actions):
        for a in actions:
            aa = "shell " + a
            print(aa)
            self.adb_cmd_sync(aa)

    def push_apk(self, package_name: str):
        self.adb_cmd_sync("shell mkdir /data/apks")
        self.adb_cmd_sync("push -p ./apks/{}.apk /data/apks/".format(package_name))

    def install_apk(self, package_name: str):
        process = Popen([self.ADB_PATH, 'shell', 'pm', 'install', '/data/apks/%s.apk' % package_name], stdout=PIPE,
                        stderr=PIPE)
        print("Package {} installation started!".format(package_name))
        return process

    def launch_app(self, package_name: str):
        process = Popen(
            [self.ADB_PATH, 'shell', 'monkey', '-p', package_name, '-c', 'android.intent.category.LAUNCHER', '1'],
            stdout=PIPE, stderr=PIPE)
        print("App", package_name, "launched!")
        return process

    def compile_app(self, package_name: str):
        process = Popen([self.ADB_PATH, 'shell', 'cmd', 'package', 'compile', '-m', 'speed-profile' '-f', package_name],
                        stdout=PIPE, stderr=PIPE)
        print("Package {} compilation started!".format(package_name))
        return process

    def query_sensor(self, location):
        result = subprocess.run([self.ADB_PATH, 'shell', 'cat', location], stdout=subprocess.PIPE)
        return int(result.stdout)

    def get_cpu_temp(self):
        return self.query_sensor(self.sensors["cpu_t"])

    def get_battery_lvl(self):
        return self.query_sensor(self.sensors["batt_lvl"])

    def get_charger_state(self):
        return True if self.query_sensor(self.sensors["charger_state"]) == 1 else False

    def get_voltage(self):
        return self.query_sensor(self.sensors["voltage"])

    def stop_app(self, package_name: str):
        self.adb_cmd_sync("shell am force-stop " + package_name)
        print("App", package_name, "stopped!")

    def setprops(self, props: dict):
        for x in props:
            self.adb_cmd_sync("shell setprop " + x + " " + props[x])
            print("{} = {}".format(x, props[x]))

    def query_all(self):
        result = subprocess.run([self.ADB_PATH, 'shell', 'sh', '/data/test.sh'], stdout=PIPE)
        return result.stdout.decode("utf-8").split("\r\n")[:-1]

    def launch_logger_on_device(self, sleep_s=1):
        self._loggerFileName = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
        logger_path = "/data/{}.txt".format(self._loggerFileName)
        self.adb_cmd_sync("shell touch {}".format(logger_path))
        result = Popen(
            [self.ADB_PATH, 'shell', 'while sleep {}; do sh /data/test.sh >> {}; done'.format(sleep_s, logger_path)],
            stdout=PIPE, stderr=PIPE)
        self._loggerPID = result
        return result

    def pull_logger_logs(self):
        if self._loggerPID:
            self._loggerPID.kill()
            self._loggerPID = None

        logger_path = "/data/{}.txt".format(self._loggerFileName)
        self.adb_cmd_sync("pull {} ./logger_{}.txt".format(logger_path, self.device_tag))
        print("Logs pulled!")
        self.adb_cmd_sync("shell rm {}".format(logger_path))
        self._loggerFileName = ""

    def push_app_profile(self, package_name) -> bool:
        dst_filename = "profiles/{}.prof".format(package_name)
        if os.path.isfile(dst_filename):
            if os.stat(dst_filename).st_size > 0:
                print("Profile for {} is known! Pushing to device...".format(dst_filename))
                self.adb_cmd_sync(
                    "push {} /data/misc/profiles/cur/0/{}/primary.prof".format(dst_filename, package_name))
                return True
        return False

    def pull_app_profile(self, package_name):
        dst_filename = "profiles/{}.prof".format(package_name)
        self.adb_cmd_sync(
            "pull /data/misc/profiles/cur/0/{}/primary.prof {}".format(package_name, dst_filename))
        if os.stat(dst_filename).st_size > 0:
            print("Profile for {} pulled!".format(package_name))
        else:
            os.remove(dst_filename)
            raise RuntimeError("Profile is empty!")

    def wait_cooldown(self, threshold=40.0, poll=10):
        print("\nCooldown!\n")
        cpu_t = 1000
        time_elapsed = 0
        self.lights_off()
        while cpu_t > threshold:
            cpu_t = int(self.get_cpu_temp()) / 1000
            print("\rCooldown! Time: {}, cpu_t: {}".format(s_to_mmss(time_elapsed), cpu_t), end="")
            if cpu_t < threshold:
                break
            time_elapsed += poll
            time.sleep(poll)
        self.lights_on()
        return

    def wait_batt_lvl(self, threshold=96, lvl_min=90, poll=10):
        batt_lvl = int(self.get_battery_lvl())
        if batt_lvl > lvl_min:
            return

        time_elapsed = 0
        print("\nCharge the battery!\n")
        notify("Charge the battery on " + self.device_tag)
        while batt_lvl < threshold:
            try:
                batt_lvl = int(self.get_battery_lvl())
                voltage = int(self.get_voltage())
            except Exception:
                batt_lvl = -1
                voltage = -1

            print("\rRechaging! Time: {}, batt_lvl: {}, voltage: {}".format(s_to_mmss(time_elapsed), batt_lvl,
                                                                            voltage / (10 ** 6)), end="")
            if batt_lvl >= threshold:
                break
            time_elapsed += poll
            time.sleep(poll)

        charger_state = self.get_charger_state()
        notified = False
        while charger_state:
            if not notified:
                notify("Disconnect the charger on " + self.device_tag)
                notified = True

            try:
                batt_lvl = int(self.get_battery_lvl())
                voltage = int(self.get_voltage())
            except Exception:
                batt_lvl = -1
                voltage = -1
            print("\rDisconnect the charger! batt_lvl: {}, voltage: {}".format(batt_lvl, voltage / (10 ** 6)), end="")
            time.sleep(2)
            charger_state = self.get_charger_state()

    def push_grabber(self):
        self.adb_cmd_sync("root")
        self.adb_cmd_sync("push test.sh /data/test.sh")
        print("Script planted!")

    def lights_off(self):
        self.adb_cmd_sync('shell "echo 0 > /sys/class/leds/lcd-backlight/brightness"')
        print("Lights off!")

    def lights_on(self):
        self.adb_cmd_sync('shell "echo 10 > /sys/class/leds/lcd-backlight/brightness"')
        print("Lights on!")


class TestBench:
    class Action(Enum):
        install = "INSTALL"
        uninstall = "UNINSTALL"
        compile = "COMPILE"
        run = "EXEC"

    stage_conf = [
        {
            "dalvik.vm.usejit": "true",
            "dalvik.vm.usejitprofiles": "true",
            "pm.dexopt.install": "speed-profile"
        },
        {
            "dalvik.vm.usejit": "false",
            "dalvik.vm.usejitprofiles": "false",
            "pm.dexopt.install": "speed"
        }]

    def __init__(self, json_path, dev_addr, dev_tag, sec_delay=1):
        self.adb_handler = ADBHandler(dev_addr, dev_tag)
        self.step_s = sec_delay
        self.adb_handler.push_grabber()
        with open(json_path) as f:
            self.benchmarks = json.load(f)
            print(self.benchmarks)

        Path("./profiles").mkdir(parents=True, exist_ok=True)
        Path("./results").mkdir(parents=True, exist_ok=True)

        print("TestBench initialized for device {}!".format(dev_tag))

    def run_test_suite(self, rounds=3, stage_1=True, stage_2=True, stage_3=True):
        for benchmark in self.benchmarks:
            apk_name = benchmark["apk"]
            if benchmark.get("skip", False):
                print("Skipping", apk_name)
                continue

            time_begin = time.time()
            print("\n\nGetting ready for", apk_name, "...")

            if stage_1:
                self.adb_handler.setprops(self.stage_conf[0])
                self.adb_handler.uninstall_apk(apk_name)
                self.measure(benchmark, self.Action.install, "JIT_A1_{}".format(self.adb_handler.device_tag))

                if self.adb_handler.push_app_profile(apk_name):
                    print("\nNo need to profile app {}!\n".format(apk_name))
                else:
                    for i in range(rounds):
                        self.measure(benchmark, self.Action.run, "BASE_A{}_{}".format(i + 1, self.adb_handler.device_tag))
                    self.adb_handler.pull_app_profile(apk_name)

                self.measure(benchmark, self.Action.compile, "JIT_A1_{}".format(self.adb_handler.device_tag))

            if stage_2:
                self.adb_handler.wait_batt_lvl(threshold=100, lvl_min=95)
                self.adb_handler.setprops(self.stage_conf[0])
                for i in range(rounds):
                    self.adb_handler.wait_cooldown()
                    self.measure(benchmark, self.Action.run, "JIT_A{}_{}".format(i + 1, self.adb_handler.device_tag))

                self.adb_handler.uninstall_apk(apk_name)
                self.adb_handler.setprops(self.stage_conf[1])
                self.measure(benchmark, self.Action.install, "AOT_A1_{}".format(self.adb_handler.device_tag))

            if stage_3:
                self.adb_handler.wait_batt_lvl(threshold=100, lvl_min=95)
                self.adb_handler.setprops(self.stage_conf[1])
                for i in range(rounds):
                    self.adb_handler.wait_cooldown()
                    self.measure(benchmark, self.Action.run, "AOT_A{}_{}".format(i + 1, self.adb_handler.device_tag))

            print("\n\nTime elapsed:", time.time() - time_begin)
            self.adb_handler.uninstall_apk(apk_name)

            if not stage_1 or not stage_2 or not stage_3:
                print("\nModified behavior! Abort....")
                break
        pass

    def measure(self, benchmark: dict, action: Action, tag="", countdown=3):
        package_name = benchmark["apk"]
        if countdown:
            self.countdown("Measuring {} for {}".format(action, package_name), countdown)

        if len(tag) > 0:
            fin_tag = "{}_{}".format(action.value, tag)
        else:
            fin_tag = action.value

        if action == self.Action.install:
            self.adb_handler.uninstall_apk(package_name)
            self._measure(benchmark, fin_tag, action=action, prolapse=3, abort_on_done=True, online_logging=False)
            if benchmark.get("manual_init", False):
                self.adb_handler.launch_app(package_name)
                notify("Manual initialization required on " + self.adb_handler.device_tag)
                input("Manual initialization required! Press ENTER to continue...")
                self.adb_handler.stop_app(package_name)
        elif action == self.Action.run:
            self._measure(benchmark, fin_tag, action=action, prolapse=3, abort_on_done=False, online_logging=False,
                          tl=benchmark.get("timeout", -1))
            self.adb_handler.stop_app(package_name)
        elif action == self.Action.compile:
            self._measure(benchmark, fin_tag, action=action, prolapse=3, abort_on_done=True, online_logging=False)

    def action_handler(self, action: Action, benchmark: dict):
        package_name = benchmark["apk"]
        print("\nExecuting action {} on package {}".format(action, package_name))
        if action == self.Action.install:
            self.adb_handler.lights_off()
            return self.adb_handler.install_apk(package_name)
        elif action == self.Action.uninstall:
            self.adb_handler.lights_off()
            self.adb_handler.uninstall_apk(package_name)
            return None
        elif action == self.Action.run:
            tmp = self.adb_handler.launch_app(package_name)
            self.adb_handler.exec_actions(benchmark.get("startup_actions", []))
            self.adb_handler.lights_off()
            return tmp
        elif action == self.Action.compile:
            self.adb_handler.lights_off()
            return self.adb_handler.compile_app(package_name)

    def _measure(self, benchmark: dict, tag: str, action: Action, prolapse=0, abort_on_done=False, online_logging=True,
                 tl=-1):
        time_elapsed = 0
        package_name = benchmark["apk"]
        filename = 'results/LOG_{}_{}.csv'.format(package_name, tag)

        if os.path.isfile(filename):
            if os.stat(filename).st_size > 0:
                print("File {} exists, skipping stage!".format(filename))
                return

        with open(filename, 'w', newline='') as csvfile:
            logwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            logwriter.writerow(['timestamp', 'current', 'voltage', 'level', 'cpu_t', 'batt_t'])

            if action:
                print("Starting measurement of action {} after {}s".format(action, prolapse))
                tmp = None
                action_finished = False
            else:
                print("Starting infinite measurement")

            if not online_logging:
                self.adb_handler.launch_logger_on_device()

            while True:
                if prolapse == time_elapsed:
                    tmp = self.action_handler(action, benchmark)

                if action and tmp:
                    tmp_retcode = tmp.poll()
                    if tmp_retcode is not None and not action_finished:
                        action_finished = True
                        action_finished_time = time_elapsed
                        print("\nAction finished [ret=%s]!" % tmp_retcode)

                    if abort_on_done and action_finished and action_finished_time + prolapse == time_elapsed:
                        print("Aborting log")
                        break

                if 0 < tl <= time_elapsed:
                    print("Time limit! Aborting log")
                    break

                try:
                    time_start = round(time.time() * 1000)
                    if online_logging:
                        vals = self.adb_handler.query_all()
                        print("\rTime: {} ({}), I: {}, V: {}, %: {}, cpu_t: {}, batt_t: {}".format(
                            s_to_mmss(time_elapsed),
                            s_to_mmss(tl),
                            int(vals[1]) / 10 ** 6,
                            int(vals[2]) / 10 ** 6,
                            vals[3],
                            int(vals[4]) / 1000,
                            int(vals[5]) / 1000),
                            end="")
                        logwriter.writerow(vals)
                    else:
                        print(
                            "\rTime: {} ({}), Offline logging enabled!".format(s_to_mmss(time_elapsed), s_to_mmss(tl)),
                            end="")
                    time_end = round(time.time() * 1000)
                    delay = self.step_s - ((time_end - time_start) / 1000)
                    if delay > 0:
                        time.sleep(delay)
                    time_elapsed += self.step_s
                except KeyboardInterrupt:
                    print("Log finished!")
                    break

            print("Saving log for", tag, "...")
            if not online_logging:
                self.adb_handler.pull_logger_logs()
                vals_arr = self.parse_logger_logs()
                for vals in vals_arr:
                    logwriter.writerow(vals)

        vals = self.adb_handler.query_all()
        print("At the end: I: {}, V: {}, %: {}, cpu_t: {}, batt_t: {}\n".format(int(vals[1]) / 10 ** 6,
                                                                                int(vals[2]) / 10 ** 6,
                                                                                vals[3],
                                                                                int(vals[4]) / 1000,
                                                                                int(vals[5]) / 1000))
        self.adb_handler.lights_on()

    def parse_logger_logs(self):
        with open("logger_{}.txt".format(self.adb_handler.device_tag)) as f:
            content = f.readlines()
        content = [x.strip() for x in content]

        vals = []

        for i in range(0, len(content), 6):
            vals.append(content[i:i + 6])
        return vals

    @staticmethod
    def parse_benchmarks_list():
        with open("benchmarks.txt") as f:
            content = f.readlines()
        return [x.strip() for x in content if x[0] != "#"]

    @staticmethod
    def countdown(tag, secs):
        print("\n")
        for i in range(secs):
            print("\r{} in {} s...".format(tag, secs - i), end="")
            time.sleep(1)
        print("\n")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        raise(RuntimeError("Usage: tester.py DEVICE_ADDRESS DEVICE_TAG"))
    else:
        device_addr = sys.argv[1]
        device_tag = sys.argv[2]

    test_bench = TestBench("benchmarks.json", device_addr, device_tag)
    test_bench.run_test_suite(rounds=20, stage_1=False, stage_2=False, stage_3=True)
    notify("Tests done on " + device_tag)
