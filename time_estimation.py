import datetime
rounds = 20
recharge = (60 * 60)
cooldown = 2 * 60

bb = {
    "com.andromeda.androbench2": 270,
    #"com.futuremark.dmandroid.application": 355,
    # "com.glbenchmark.GLBenchmark11": 450,
    #"com.primatelabs.geekbench5": 920,
    #"com.futuremark.pcmark.android.benchmark": 520,
    #"com.rightware.BasemarkOSII": 340,
    "com.quicinc.vellamo": 490
}

if __name__ == "__main__":
    total_time = 0

    for x in bb:
        total_time += (recharge + (bb[x] + cooldown) * rounds) * 2


    a = datetime.timedelta(seconds=total_time)
    print(a)
